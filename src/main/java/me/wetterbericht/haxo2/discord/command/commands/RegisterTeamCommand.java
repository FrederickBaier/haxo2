package me.wetterbericht.haxo2.discord.command.commands;

import me.wetterbericht.haxo2.discord.DiscordBotMain;
import me.wetterbericht.haxo2.discord.command.CommandExecutor;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

public class RegisterTeamCommand implements CommandExecutor {
    @Override
    public void onCommand(User user, MessageChannel channel, String[] args) {
        DiscordBotMain.getInstance().getSetupManager().startSetup(user);
    }
}
