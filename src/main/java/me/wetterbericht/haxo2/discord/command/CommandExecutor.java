package me.wetterbericht.haxo2.discord.command;

import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

public interface CommandExecutor {

    void onCommand(User user, MessageChannel channel, String[] args);

}
