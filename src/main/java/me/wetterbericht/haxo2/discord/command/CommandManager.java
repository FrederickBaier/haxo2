package me.wetterbericht.haxo2.discord.command;


import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

import java.util.HashMap;

public class CommandManager implements ICommandManager{

    private HashMap<String, CommandExecutor> commands = new HashMap<>();


    @Override
    public boolean registerCommand(String command, CommandExecutor executor) {
        commands.put(command, executor);
        return false;
    }

    @Override
    public boolean unregisterCommand(String command) {
        return commands.remove(command) != null;
    }

    @Override
    public void onCommand(User user, MessageChannel messageChannel, String command, String[] args) {
        System.out.println("Command: " + command);
        String string = commands.keySet().stream().filter(s -> s.equalsIgnoreCase(command)).findFirst().orElse(null);
        if (string == null){
            messageChannel.sendMessage("Command nicht gefunden!") .queue();
            return;
        }
        commands.get(string).onCommand(user, messageChannel, args);

    }
}
