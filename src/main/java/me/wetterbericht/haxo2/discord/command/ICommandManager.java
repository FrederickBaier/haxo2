package me.wetterbericht.haxo2.discord.command;

import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

public interface ICommandManager {

    /**
     *  Registers a Command
     * @param command
     * @param executor
     * @return false if the command is already exists
     */
    boolean registerCommand(String command, CommandExecutor executor);

    /**
     *
     * @param command
     * @return true if the command was registered
     */
    boolean unregisterCommand(String command);

    /**
     *  Called when a User entered a command
     * @param user
     * @param messageChannel
     * @param command
     * @param args
     */
    void onCommand(User user, MessageChannel messageChannel, String command, String[] args);

}
