package me.wetterbericht.haxo2.discord;


import lombok.Getter;
import me.wetterbericht.haxo2.discord.command.CommandManager;
import me.wetterbericht.haxo2.discord.command.ICommandManager;
import me.wetterbericht.haxo2.discord.command.commands.RegisterTeamCommand;
import me.wetterbericht.haxo2.discord.listener.PrivateMessageReceivedListener;
import me.wetterbericht.haxo2.discord.listener.ReadyListener;
import me.wetterbericht.haxo2.discord.teamsetup.TeamSetupManager;
import me.wetterbericht.haxo2.discord.util.BotUtils;
import me.wetterbericht.haxo2.discord.util.JsonData;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;


import javax.security.auth.login.LoginException;
import java.io.File;

public class DiscordBotMain {

    @Getter
    private static DiscordBotMain instance;
    @Getter
    private JDA jda;
    @Getter
    private ICommandManager commandManager;
    @Getter
    private TeamSetupManager setupManager = new TeamSetupManager();


    public DiscordBotMain() {
        File file = new File("plugins/Haxo/bot_token");
        if (!file.exists()){
            new JsonData().append("token", "PUT_TOKEN_HERE").append("guild", (long)10).saveAsFile(file);
            return;
        }
        JsonData jsonData = JsonData.fromJsonFile(file);
        BotUtils.TOKEN = jsonData.getString("token");
        BotUtils.GUILD_ID = jsonData.getLong("guild");
        instance = this;
        commandManager = new CommandManager();
        commandManager.registerCommand("registerTeam", new RegisterTeamCommand());
        JDABuilder builder = new JDABuilder(AccountType.BOT);
        builder.setToken(BotUtils.TOKEN);
        builder.setAutoReconnect(true);
        builder.setStatus(OnlineStatus.ONLINE);
        builder.addEventListeners(new ReadyListener());
        builder.addEventListeners(new PrivateMessageReceivedListener());
        try {
            jda = builder.build();
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }



    public static void main(String[] args) {
        new DiscordBotMain();
    }
}
