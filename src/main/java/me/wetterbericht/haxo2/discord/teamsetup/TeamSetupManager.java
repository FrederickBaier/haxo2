package me.wetterbericht.haxo2.discord.teamsetup;

import me.wetterbericht.haxo2.bukkit.*;
import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import me.wetterbericht.haxo2.discord.DiscordBotMain;
import me.wetterbericht.haxo2.discord.util.BotUtils;
import me.wetterbericht.haxo2.discord.util.ThreadUtils;
import me.wetterbericht.haxo2.discord.util.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class TeamSetupManager {

    private ArrayList<TeamSetupData> teamSetupDatas = new ArrayList<>();

    public void startSetup(User user) {
        if (Varo.getInstance().getVaroConfig().getVaroState() != VaroState.REGISTER_PLAYER) {
            user.openPrivateChannel().complete().sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Du kannst dich derzeit nicht registrieren.").build()).queue();
            return;
        }
        if (Varo.getInstance().getVaroConfig().isUserInAnyTeam(user.getIdLong())) {
            user.openPrivateChannel().complete().sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Du bist bereits registriert.").build()).queue();
            return;
        }
        if (Varo.getInstance().getVaroConfig().getSpawnLocations().size() == Varo.getInstance().getVaroConfig().getVaroTeams().size() * 2) {
            user.openPrivateChannel().complete().sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Haxo ist leider schon voll.").build()).queue();
            return;
        }
        teamSetupDatas.add(new TeamSetupData().setDiscordUser1(user).setSetupState(TeamSetupState.TEAM_NAME));
        PrivateChannel privateChannel = user.openPrivateChannel().complete();
        if (!BotUtils.isUserMemberofTheGuild(user)) {
            privateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Du bist nicht auf dem Discord.").build()).queue();
            return;
        }
        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.YELLOW).setTitle("Schön, dass du bei Haxo2 mitmachen willst.").build()).queue();
        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.YELLOW).setTitle("Du kannst das Setup jeder zeit mit \"abort\" stoppen.").build()).queue();
        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.CYAN).setTitle("Bitte schreibe mir den Namen deines Teams.").build()).queue();
    }

    public void privateMessageReceived(User user, String message) {
        if (message == null || message.equals(""))
            return;
        PrivateChannel privateChannel = user.openPrivateChannel().complete();
        TeamSetupData teamSetupData = getFromFirstUser(user);
        if (teamSetupData != null) {
            if (message.equalsIgnoreCase("abort")) {
                abortSetup(teamSetupData, privateChannel);
                return;
            }
            switch (teamSetupData.getSetupState()) {
                case TEAM_NAME:
                    if (message.length() > 10){
                        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Der Name darf nicht länger sein als 10 Zeichen.").build()).queue();
                        return;
                    }
                    if (Varo.getInstance().getVaroConfig().getVaroTeamByName(message) != null) {
                        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Dieser Name ist bereits vergeben.").build()).queue();
                        return;
                    }
                    teamSetupData.setName(message);
                    privateChannel.sendMessage(new EmbedBuilder().setColor(Color.CYAN).setTitle("Bitte schreibe mir nun die Farbe deines Teams. (" + StringUtils.join(Utils.getColors(), ", ") + ")").build()).queue();
                    teamSetupData.setSetupState(TeamSetupState.TEAM_COLOR);
                    break;
                case TEAM_COLOR:
                    Color color = Utils.getColorByName(message);
                    if (color == null) {
                        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Diese Farbe gibt es nicht").build()).queue();
                        return;
                    }
                    teamSetupData.setColor(color);
                    privateChannel.sendMessage(new EmbedBuilder().setColor(Color.CYAN).setTitle("Bitte schreibe mir nun den Discord-Namen von deinem Mate.").build()).queue();
                    teamSetupData.setSetupState(TeamSetupState.DISCORD_MATE);
                    break;
                case DISCORD_MATE:
                    List<User> users = DiscordBotMain.getInstance().getJda().getUsersByName(message, false).stream().filter(mate -> BotUtils.isUserMemberofTheGuild(mate)).collect(Collectors.toList());
                    if (users.size() == 0) {
                        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Dein Mate muss auf dem Discord-Server von Haxo 2 sein.").build()).queue();
                        return;
                    }
                    if (users.size() > 1) {
                        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Es darf nicht 2 Leute mit dem gleichen Namen auf dem Discord-Server geben.").build()).queue();
                        return;
                    }
                    User mateUser = users.get(0);
                    if (mateUser.equals(user)) {
                        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Du kannst nicht mit dir selber teamen. :stuck_out_tongue_winking_eye:").build()).queue();
                        return;
                    }
                    if (mateUser.equals(mateUser.getJDA().getSelfUser())){
                        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Leider darf ich nicht mitmachen. Aber ich finde es schön, dass du mit mir teamen wollen würdest. :blush:").build()).queue();
                        return;
                    }
                    if (isUserInSetup(mateUser)) {
                        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Dein Mate ist gerade in einem Setup.").build()).queue();
                        return;
                    }
                    teamSetupData.setDiscordUser2(mateUser);
                    PrivateChannel matePrivateChannel = mateUser.openPrivateChannel().complete();
                    UUID verifyCodeUser1 = UUID.randomUUID();
                    UUID verifyCodeUser2 = UUID.randomUUID();
                    teamSetupData.setVerifyCodeUser1(verifyCodeUser1);
                    teamSetupData.setVerifyCodeUser2(verifyCodeUser2);
                    matePrivateChannel.sendMessage(new EmbedBuilder().setColor(Color.CYAN).setTitle(user.getName() + " möchte mit dir in Haxo 2 teamen.").build()).queue();
                    matePrivateChannel.sendMessage(new EmbedBuilder().setColor(Color.GREEN).setTitle("Wenn du mit " + user.getName() + " teamen möchtest begebe dich auf den Mincraftserver und schreibe diesen Code in den Chat: " + verifyCodeUser2).build()).queue();
                    matePrivateChannel.sendMessage(new EmbedBuilder().setColor(Color.CYAN).setTitle("Wenn du nicht mit " + user.getName() + " teamen möchtest schreibe mir auf Discord \"abort\"").build()).queue();
                    privateChannel.sendMessage(new EmbedBuilder().setColor(Color.CYAN).setTitle("Bitte begebe dich auf den Mincraftserver und schreibe diesen Code in den Chat: " + verifyCodeUser1).build()).queue();
                    teamSetupData.setSetupState(TeamSetupState.MC_CODE);
                    break;
                case MC_CODE:
                    //DO nothing
            }
        } else {
            teamSetupData = getFomSecondUser(user);
            if (teamSetupData != null) {
                if (message.equalsIgnoreCase("abort")) {
                    PrivateChannel firstUserChannel = teamSetupData.getDiscordUser1().openPrivateChannel().complete();
                    firstUserChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle(user.getName() + " will nicht mit dir teamen. Das Setup wurde abgebrochen. ").build()).queue();
                    abortSetup(teamSetupData, privateChannel);
                    return;
                }
            }
        }
    }

    public void onMcVerifyCodeAsync(Player p, UUID verifyCode) {
        ThreadUtils.startNormalThread(() -> onMcVerifyCode(p, verifyCode));
    }

    public void onMcVerifyCode(Player p, UUID verifyCode) {
        User user = null;
        TeamSetupData teamSetupData = null;
        for (TeamSetupData allTeams : teamSetupDatas) {
            user = allTeams.getUserFromVerifyCode(verifyCode);
            teamSetupData = allTeams;
            if (user != null)
                break;
        }
        if (user == null) {
            Bukkit.getScheduler().runTask(BukkitPluginMain.getInstance(), () -> p.sendMessage(Varo.PREFIX + "Ungültiger Code."));
            return;
        }
        PrivateChannel userPrivateChannel = user.openPrivateChannel().complete();
        if (teamSetupData.getDiscordUser1().equals(user)) {
            teamSetupData.setMinecraftUUID1(p.getUniqueId());
        } else {
            teamSetupData.setMinecraftUUID2(p.getUniqueId());
        }
        Bukkit.getScheduler().runTask(BukkitPluginMain.getInstance(), () -> p.kickPlayer(Varo.PREFIX + "Du bist jetzt registriert."));
        ;
        if (teamSetupData.getMinecraftUUID1() == null || teamSetupData.getMinecraftUUID2() == null) {
            userPrivateChannel.sendMessage(new EmbedBuilder().setColor(Color.GREEN).setTitle("Vielen Dank. Es wird noch auf die Antwort deines Mates gewartet.").build()).queue();
            return;
        }
        PrivateChannel otherUserPrivateChannel = teamSetupData.getOtherUser(user).openPrivateChannel().complete();
        if (Varo.getInstance().getVaroConfig().getVaroTeamByName(teamSetupData.getName()) != null) {
            userPrivateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED).setTitle("Dieser Name ist bereits vergeben.").build()).queue();
            abortSetup(teamSetupData, userPrivateChannel);
            otherUserPrivateChannel.sendMessage(new EmbedBuilder().setColor(Color.RED)
                    .setTitle("Das Setup wurde abgebrochen, weil der Name bereits vergeben war.").build()).queue();
            return;
        }
        userPrivateChannel.sendMessage(new EmbedBuilder().setColor(Color.GREEN)
                .setTitle("Vielen Dank. Setup abgeschlossen.").build()).queue();
        //Message to mate
        otherUserPrivateChannel.sendMessage(new EmbedBuilder().setColor(Color.GREEN)
                .setTitle("Setup abgeschlossen.").build()).queue();
        teamSetupDatas.remove(teamSetupData);
        completeSetup(teamSetupData);
    }

    private void completeSetup(TeamSetupData teamSetupData) {
        Guild guild = BotUtils.getTheGuild(DiscordBotMain.getInstance().getJda());
        //check if role already exist
        if (!guild.getRolesByName(teamSetupData.getName(), true).isEmpty()) {
            teamSetupData.getDiscordUser1().openPrivateChannel().complete().sendMessage(new EmbedBuilder().setColor(Color.RED)
                    .setTitle("Eine Discord-Rolle mit selbigen Namen existiert bereits. Bitte versuche das Setup erneut und wähle einen anderen Namen.").build()).queue();
            teamSetupData.getDiscordUser2().openPrivateChannel().complete().sendMessage(new EmbedBuilder().setColor(Color.RED)
                    .setTitle("Eine Discord-Rolle mit selbigen Namen existiert bereits. Bitte versuche das Setup erneut und wähle einen anderen Namen.").build()).queue();
            return;
        }
        //check if both users are member of the guild
        if (!BotUtils.isUserMemberofTheGuild(teamSetupData.getDiscordUser1()) || !BotUtils.isUserMemberofTheGuild(teamSetupData.getDiscordUser2())) {
            teamSetupData.getDiscordUser1().openPrivateChannel().complete().sendMessage(new EmbedBuilder().setColor(Color.RED)
                    .setTitle("Ein User hat den Haxo 2 Discord verlassen.").build()).queue();
            teamSetupData.getDiscordUser2().openPrivateChannel().complete().sendMessage(new EmbedBuilder().setColor(Color.RED)
                    .setTitle("Ein User hat den Haxo 2 Discord verlassen.").build()).queue();
            return;
        }
        VaroTeam varoTeam = new VaroTeam(teamSetupData.getName(), teamSetupData.getColor());
        varoTeam.setVaroPlayer1(new VaroPlayer(teamSetupData.getDiscordUser1().getIdLong(), teamSetupData.getMinecraftUUID1()));
        varoTeam.setVaroPlayer2(new VaroPlayer(teamSetupData.getDiscordUser2().getIdLong(), teamSetupData.getMinecraftUUID2()));
        Varo.getInstance().getVaroConfig().addVaroTeam(varoTeam);
        Varo.getInstance().saveVaroConfig();

        Role role = guild.getController().createRole().setHoisted(true).setName(teamSetupData.getName()).setColor(teamSetupData.getColor()).complete();
        guild.getController().addSingleRoleToMember(guild.getMember(teamSetupData.getDiscordUser1()), role).queue();
        guild.getController().addSingleRoleToMember(guild.getMember(teamSetupData.getDiscordUser2()), role).queue();
        guild.getRoles().forEach(allRoles-> System.out.println(allRoles.getName() + " : " + allRoles.getPosition() + " : " + allRoles.getPositionRaw()));
        while (true) {
            try {
                guild.getController().modifyRolePositions().selectPosition(role).moveUp(1).complete();
            } catch (Exception e){
                break;
            }
        }
        BotUtils.getTheUser(guild.getJDA()).openPrivateChannel().complete().sendMessage(new EmbedBuilder().setColor(Color.ORANGE).setTitle("Neues Team: " + teamSetupData.getName())
                .setDescription(teamSetupData.getDiscordUser1().getName() + " mit " + teamSetupData.getMinecraftUUID1() + ", " + teamSetupData.getDiscordUser2().getName() + " mit " + teamSetupData.getMinecraftUUID2()).build()).queue();
    }

    private int getHeighestRolePosition(Guild guild){
        return guild.getRoles().stream().reduce((role, role2) -> role.getPosition() > role2.getPosition() ? role : role2).orElse(null).getPosition();
    }

    private void abortSetup(TeamSetupData teamSetupData, PrivateChannel privateChannel) {
        this.teamSetupDatas.remove(teamSetupData);
        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.CYAN).setTitle("Setup abgebrochen.").build()).queue();
        privateChannel.sendMessage(new EmbedBuilder().setColor(Color.YELLOW).setTitle("Du kannst das Setup jederzeit mit \"-registerteam\" erneut starten.").build()).queue();
        if (teamSetupData.getDiscordUser2() != null){
            if (teamSetupData.getDiscordUser2().equals(privateChannel.getUser())){
                privateChannel = teamSetupData.getDiscordUser1().openPrivateChannel().complete();
                privateChannel.sendMessage(new EmbedBuilder().setColor(Color.CYAN).setTitle("Setup abgebrochen.").build()).queue();
                privateChannel.sendMessage(new EmbedBuilder().setColor(Color.YELLOW).setTitle("Du kannst das Setup jederzeit mit \"-registerteam\" erneut starten.").build()).queue();
            } else {
                privateChannel = teamSetupData.getDiscordUser2().openPrivateChannel().complete();
                privateChannel.sendMessage(new EmbedBuilder().setColor(Color.CYAN).setTitle("Setup abgebrochen.").build()).queue();
                privateChannel.sendMessage(new EmbedBuilder().setColor(Color.YELLOW).setTitle("Du kannst das Setup jederzeit mit \"-registerteam\" erneut starten.").build()).queue();
            }
        }
    }


    private TeamSetupData getFromFirstUser(User user) {
        return teamSetupDatas.stream().filter(teamSetupData -> teamSetupData.getDiscordUser1().equals(user)).findFirst().orElse(null);
    }

    private TeamSetupData getFomSecondUser(User user) {
        return teamSetupDatas.stream().filter(teamSetupData -> teamSetupData.getDiscordUser2().equals(user)).findFirst().orElse(null);
    }

    public boolean isUserInSetup(User user) {
        return teamSetupDatas.stream().
                anyMatch(teamSetupData -> teamSetupData.getDiscordUser1().equals(user) || (teamSetupData.getDiscordUser2() != null && teamSetupData.getDiscordUser2().equals(user)));
    }

    public boolean isPlayerRegistered(UUID uuid){
        for (TeamSetupData teamSetupData : teamSetupDatas){
            if ((teamSetupData.getMinecraftUUID1() != null && teamSetupData.getMinecraftUUID1().equals(uuid)) || (teamSetupData.getMinecraftUUID2() != null && teamSetupData.getMinecraftUUID2().equals(uuid)))
                return true;
        }
        return false;
    }

}
