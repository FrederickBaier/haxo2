package me.wetterbericht.haxo2.discord.teamsetup;

import lombok.Getter;
import net.dv8tion.jda.api.entities.User;

import java.awt.*;
import java.util.UUID;

@Getter
public class TeamSetupData {

    private User discordUser1;

    private User discordUser2;

    private UUID minecraftUUID1;

    private UUID minecraftUUID2;

    private UUID verifyCodeUser1;

    private UUID verifyCodeUser2;

    private TeamSetupState setupState;

    private String name;

    private Color color;

    public TeamSetupData setDiscordUser1(User discordUser1) {
        this.discordUser1 = discordUser1;
        return this;
    }

    public TeamSetupData setDiscordUser2(User discordUser2) {
        this.discordUser2 = discordUser2;
        return this;
    }

    public TeamSetupData setVerifyCodeUser1(UUID verifyCodeUser1) {
        this.verifyCodeUser1 = verifyCodeUser1;
        return this;
    }

    public TeamSetupData setVerifyCodeUser2(UUID verifyCodeUser2) {
        this.verifyCodeUser2 = verifyCodeUser2;
        return this;
    }

    public TeamSetupData setMinecraftUUID1(UUID minecraftUUID1) {
        this.minecraftUUID1 = minecraftUUID1;
        return this;
    }

    public TeamSetupData setMinecraftUUID2(UUID minecraftUUID2) {
        this.minecraftUUID2 = minecraftUUID2;
        return this;
    }

    public TeamSetupData setName(String name) {
        this.name = name;
        return this;
    }

    public TeamSetupData setColor(Color color) {
        this.color = color;
        return this;
    }

    public TeamSetupData setSetupState(TeamSetupState setupState) {
        this.setupState = setupState;
        return this;
    }

    private long createTimeStamp = System.currentTimeMillis();

    /**
     *
     * @param verifyCode
     * @return the user which was send this verify code and null if the verify code was not sent to any user of this team
     */
    public User getUserFromVerifyCode(UUID verifyCode){
        if (verifyCodeUser1.equals(verifyCode))
            return discordUser1;
        if (verifyCodeUser2.equals(verifyCode))
            return discordUser2;
        return null;
    }

    public User getOtherUser(User user) {
        if (user.equals(discordUser1))
            return discordUser2;
        if (user.equals(discordUser2))
            return discordUser1;
        return null;
    }


}
