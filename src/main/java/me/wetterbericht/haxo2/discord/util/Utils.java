package me.wetterbericht.haxo2.discord.util;

import org.apache.commons.lang.StringUtils;

import java.awt.*;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static Color getColorByName(String name) {
        try {
            return (Color)Color.class.getField(name.toUpperCase()).get(null);
        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
            return null;
        }
    }

    public static ArrayList<String> getColors() {
        ArrayList<String> list = new ArrayList<>();
       for (Field field : Color.class.getDeclaredFields()){
           if (StringUtils.isAllUpperCase(field.getName())){
               if (field.getName().equalsIgnoreCase("FACTOR"))
                   continue;
               list.add(field.getName());
           }
       }
       return list;
    }



}
