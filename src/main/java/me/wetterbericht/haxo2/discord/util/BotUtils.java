package me.wetterbericht.haxo2.discord.util;

import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.discord.DiscordBotMain;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import org.bukkit.entity.Player;

public class BotUtils {

    public static long GUILD_ID;

    public static String TOKEN;

    public static Guild getTheGuild(JDA jda) {
        return jda.getGuildById(GUILD_ID);
    }

    public static User getTheUser(JDA jda) {
        return jda.getUserById(335120906869407747L);
    }

    public static boolean isUserMemberofTheGuild(User user) {
        return user.getMutualGuilds().contains(getTheGuild(DiscordBotMain.getInstance().getJda()));
    }

    public static void sendMessageInGuild(String textChannel, MessageEmbed msg) {
        BotUtils.getTheGuild(DiscordBotMain.getInstance().getJda()).getTextChannelsByName(textChannel, true).get(0).sendMessage(msg).queue();
    }

    public static void sendMessageInGuild(String textChannel, String msg) {
        BotUtils.getTheGuild(DiscordBotMain.getInstance().getJda()).getTextChannelsByName(textChannel, true).get(0).sendMessage(msg).queue();
    }

    public static void sendStrikeMessage(Player p, VaroPlayer varoPlayer) {
        switch (varoPlayer.getStrikes()) {
            case 1:
                sendMessageInGuild("strike-infos", new EmbedBuilder().setTitle(p.getName() + " (" + varoPlayer.getDiscordUser().getName() + ") hat einen Strike erhalten. (1)")
                        .setDescription("Seine Koordinaten: X:" + p.getLocation().getBlockX() + " Y:" + p.getLocation().getBlockY() + " Z:" + p.getLocation().getBlockZ()).build());
                break;
            case 2:
                sendMessageInGuild("strike-infos", new EmbedBuilder().setTitle(p.getName() + " (" + varoPlayer.getDiscordUser().getName() + ") hat einen Strike erhalten. (2)")
                        .setDescription("Seine Inventar wurde geleert.").build());
                break;
            case 3:
                sendMessageInGuild("strike-infos", new EmbedBuilder().setTitle(p.getName() + " (" + varoPlayer.getDiscordUser().getName() + ") hat einen Strike erhalten. (3)")
                        .setDescription("Er ist aus dem Projekt ausgeschieden.").build());
                break;
        }

    }

}
