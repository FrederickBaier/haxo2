package me.wetterbericht.haxo2.discord.util;

public class ThreadUtils {

    public static Thread startNormalThread(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.start();
        return thread;
    }

    public static Thread startDeamonThread(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.start();
        return thread;
    }

}