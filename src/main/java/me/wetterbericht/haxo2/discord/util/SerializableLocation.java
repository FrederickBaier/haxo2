package me.wetterbericht.haxo2.discord.util;

import com.google.common.base.Preconditions;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;

@Getter
@AllArgsConstructor
public class SerializableLocation {

    public SerializableLocation(Location location){
        this(location.getWorld().getName(), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }

    public SerializableLocation(String worldName, double x, double y, double z){
        this(worldName, x, y, z, 0, 0);
    }

    private String worldName;

    private double x;
    private double y;
    private double z;
    private  float yaw;
    private float pitch;


    public Location toBukkitLocation(){
        return new Location(Preconditions.checkNotNull(Bukkit.getWorld(worldName)), x, y, z, yaw, pitch);
    }

}
