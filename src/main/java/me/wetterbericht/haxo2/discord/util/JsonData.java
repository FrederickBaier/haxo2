package me.wetterbericht.haxo2.discord.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class JsonData {

	private final static Gson GSON = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
	
	private JsonObject jsonObject = new JsonObject();

	public JsonData() { }
	
	public JsonData(JsonObject jsonObject) {
		this.jsonObject = jsonObject;
	}
	
	public static JsonData fromJsonFile(String path) {
		return fromJsonFile(new File(path));
	}
	
	public static JsonData fromJsonFile(File file) {
		return fromJsonString(loadFile(file));
	}
	
	public static JsonData fromJsonString(String string) {
		JsonObject jsonObject = GSON.fromJson(string, JsonObject.class);
		return new JsonData(jsonObject);
	}
	
	public static void saveObjectAsFile(File file, Object object) {
		saveJsonElementAsFile(file, GSON.toJsonTree(object));
	}
	
	private static String loadFile(File file) {
		if (!file.exists())
			return "";
		try {
		FileInputStream fis = new FileInputStream(file);
		byte[] data = new byte[(int) file.length()];
		fis.read(data);
		fis.close();

		return new String(data, "UTF-8");
		} catch (IOException e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public JsonData append(String property, String value) {
		if (property == null)
			return this;
		jsonObject.addProperty(property, value);
		return this;
	}
	
	public JsonData append(String property, Object value) {
		if (property == null)
			return this;
		jsonObject.add(property, GSON.toJsonTree(value));
		return this;
	}

	public JsonData append(String property, Number value) {
		if (property == null)
			return this;
		jsonObject.addProperty(property, value);
		return this;
	}

	public JsonData append(String property, Boolean value) {
		if (property == null)
			return this;
		jsonObject.addProperty(property, value);
		return this;
	}
	
	public int getInt(String property) {
		if (!jsonObject.has(property)) return -1;
		return jsonObject.get(property).getAsInt();
	}
	
	public long getLong(String property) {
		if (!jsonObject.has(property)) return -1;
		return jsonObject.get(property).getAsLong();
	}
	
	public Boolean getBoolean(String property) {
		if (!jsonObject.has(property)) return false;
		return jsonObject.get(property).getAsBoolean();
	}
	
	public <T> T getObject(String property, Class<T> clazz) {
		if (!jsonObject.has(property)) return null;
		return GSON.fromJson(jsonObject.get(property), clazz);
	}
	
	public String getString(String property) {
		if (!jsonObject.has(property)) return null;
		return jsonObject.get(property).getAsString();
	}
	
	
	public void saveAsFile(String path) {
		saveJsonElementAsFile(new File(path), jsonObject);
	}
	
	public void saveAsFile(File file) {
		saveJsonElementAsFile(file, jsonObject);
	}
	
	public static boolean saveJsonElementAsFile(String path, JsonElement jsonObject) {
		return saveJsonElementAsFile(new File(path), jsonObject);
	}

	public static boolean saveJsonElementAsFile(File file, JsonElement jsonObject) {
		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
			String jsonString = GSON.toJson(jsonObject);
			outputStreamWriter.write(jsonString);
			outputStreamWriter.flush();
			outputStreamWriter.close();
		} catch (IOException e) {
			return false;
		}
		return false;
	}
	
	public String getAsJsonString() {
		return GSON.toJson(jsonObject);
	}
	
	public byte[] getJsonStringAsBytes() {
		return getAsJsonString().getBytes(StandardCharsets.UTF_8);
	}
}
