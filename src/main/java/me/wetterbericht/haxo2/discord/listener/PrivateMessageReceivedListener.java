package me.wetterbericht.haxo2.discord.listener;

import me.wetterbericht.haxo2.discord.DiscordBotMain;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.Arrays;

public class PrivateMessageReceivedListener extends ListenerAdapter {

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
        if (event.getAuthor().equals(event.getJDA().getSelfUser()))
            return;
        String msg = event.getMessage().getContentDisplay();
        if (DiscordBotMain.getInstance().getSetupManager().isUserInSetup(event.getAuthor())){
            DiscordBotMain.getInstance().getSetupManager().privateMessageReceived(event.getAuthor(), msg);
            return;
        }
        if (msg.startsWith("-")){
            msg = msg.replaceFirst("-", "");
            String[] args = msg.split(" ");
            if (args.length == 0)
                return;
            if (args.length == 1){
                DiscordBotMain.getInstance().getCommandManager().onCommand(event.getAuthor(), event.getChannel(), args[0], Arrays.copyOfRange(args, 1, args.length));
            } else {
                DiscordBotMain.getInstance().getCommandManager().onCommand(event.getAuthor(), event.getChannel(), args[0], new String[]{});
            }
        }
        /*
        TextChannel textChannel = BotUtils.getTheGuild(event.getJDA()).getTextChannels().stream().filter(text -> text.getName().equalsIgnoreCase("botchat")).findFirst().orElse(null);
        if (textChannel != null) {
            textChannel.sendMessage(event.getAuthor().getAsMention() + " hat eine Nachricht geschrieben.").queue();
        }
        */
    }
}
