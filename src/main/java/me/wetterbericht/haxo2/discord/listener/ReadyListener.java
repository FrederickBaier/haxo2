package me.wetterbericht.haxo2.discord.listener;

import me.wetterbericht.haxo2.discord.util.BotUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;

public class ReadyListener extends ListenerAdapter {

    @Override
    public void onReady(ReadyEvent event) {

        BotUtils.getTheUser(event.getJDA()).openPrivateChannel().complete().sendMessage(new EmbedBuilder().setColor(Color.ORANGE).setTitle("Ich bin wieder da").build()).queue();

    }
}


