package me.wetterbericht.haxo2.bukkit.scoreboardapi;

import lombok.Getter;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.scoreboard.CraftScoreboard;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class SimpleScoreboard {

    private String title;
    private Scoreboard scoreboard = new Scoreboard();
    private ScoreboardObjective objective;

    private List<Player> viewers = new ArrayList<>();
    private List<ScoreboardEntry> entries = new ArrayList<>();

    private TeamNameManager teamNameManager = new TeamNameManager();

    public SimpleScoreboard(String title) {
        this.title = title;
        this.objective = scoreboard.registerObjective("bbb", new ScoreboardBaseCriteria("aaa"));
        this.objective.setDisplayName(this.title);
    }

    public SimpleScoreboard() {
        this.objective = scoreboard.registerObjective(" ", new ScoreboardBaseCriteria("aaa"));
    }

    public void setTitle(String title) {
        this.title = title;
        this.objective.setDisplayName(this.title);
        sendPacketToViewers(new PacketPlayOutScoreboardObjective(this.objective, 2));
    }

    public ScoreboardEntry addScoreboardEntry(int score, String text, String changeableText) {
        ScoreboardEntry scoreboardEntry = new ScoreboardEntry(this, this.teamNameManager.getNewTeamName(), text, changeableText, score);
        this.entries.add(scoreboardEntry);
        sendPacketToViewers(scoreboardEntry.getTeamAddPacket());
        sendPacketToViewers(scoreboardEntry.getAddAndScoreChangePacket());
        return scoreboardEntry;
    }

    public ScoreboardEntry addScoreboardEntry(int score, String text) {
        ScoreboardEntry scoreboardEntry = new ScoreboardEntry(this, this.teamNameManager.getNewTeamName(), text, "", score);
        this.entries.add(scoreboardEntry);
        sendPacketToViewers(scoreboardEntry.getTeamAddPacket());
        sendPacketToViewers(scoreboardEntry.getAddAndScoreChangePacket());
        return scoreboardEntry;
    }

    public void removeScoreboardEntry(ScoreboardEntry scoreboardEntry) {
        this.entries.remove(scoreboardEntry);
        this.teamNameManager.setTeamNameUnused(scoreboardEntry.getTeamName());
        sendPacketToViewers(scoreboardEntry.getRemovePacket());
        sendPacketToViewers(scoreboardEntry.getTeamRemovePacket());
    }


    public List<ScoreboardEntry> getScoreboardEntries(String text) {
        return this.entries.stream().filter(scoreboardEntry -> scoreboardEntry.getText().equals(text)).collect(Collectors.toList());
    }

    public List<ScoreboardEntry> getScoreboardEntries(int score) {
        return this.entries.stream().filter(scoreboardEntry -> scoreboardEntry.getScore() == score).collect(Collectors.toList());
    }

    public ScoreboardEntry getFirstScoreboardEntry(int score){
        List<ScoreboardEntry> scoreboardEntries = getScoreboardEntries(score);
        return scoreboardEntries.size() == 0 ? null : scoreboardEntries.get(0);
    }

    public ScoreboardEntry getFirstScoreboardEntry(String text){
        List<ScoreboardEntry> scoreboardEntries = getScoreboardEntries(text);
        return scoreboardEntries.size() == 0 ? null : scoreboardEntries.get(0);
    }



    public void sendSidebar(Player p) {
        if (this.viewers.contains(p)) return;
        loadSidebar(p);
        this.viewers.add(p);
    }

    private void loadSidebar(Player p){
        PacketPlayOutScoreboardObjective packet = new PacketPlayOutScoreboardObjective(this.objective, 0);
        PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, this.objective);
        sendPacket(p, packet);
        sendPacket(p, display);
        this.entries.forEach(scoreboardEntry -> sendPacket(p, scoreboardEntry.getTeamAddPacket()));
        this.entries.forEach(scoreboardEntry -> sendPacket(p, scoreboardEntry.getAddAndScoreChangePacket()));
    }

    public void destroySidebar(Player p) {
        sendPacket(p,  new PacketPlayOutScoreboardObjective(this.objective, 1));
        this.entries.forEach(scoreboardEntry -> sendPacket(p, scoreboardEntry.getTeamRemovePacket()));
        this.viewers.remove(p);
    }


    public void sendPacket(Player player, Packet packet) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    public void sendPacketToViewers(Packet packet) {
        this.viewers.forEach(player -> sendPacket(player, packet));
    }
}
