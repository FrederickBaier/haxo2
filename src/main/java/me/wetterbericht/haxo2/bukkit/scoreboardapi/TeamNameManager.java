package me.wetterbericht.haxo2.bukkit.scoreboardapi;

import java.util.ArrayList;

public class TeamNameManager {

    private final String[] COLORS = new String[]{"§0", "§1", "§2", "§3", "§4", "§5", "§6", "§7", "§8", "§9", "§a", "§b", "§c", "§d", "§e", "§f"};

    private ArrayList<String> usedTeamNames = new ArrayList<>();



    public void setTeamNameUnused(String teamName){
        this.usedTeamNames.remove(teamName);
    }

    public String getNewTeamName(){
        String teamName = COLORS[0] + COLORS[0];
        int i1 = 0;
        int i2 = 0;
        while (usedTeamNames.contains(teamName)){
            i2++;
            if (i2 == COLORS.length){
                i2 = 0;
                i1++;
            }
            teamName = COLORS[i1] + COLORS[i2];
        }
        usedTeamNames.add(teamName);
        return teamName;
    }


}
