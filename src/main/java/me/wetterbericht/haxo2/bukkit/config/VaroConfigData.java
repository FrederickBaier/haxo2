package me.wetterbericht.haxo2.bukkit.config;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import me.wetterbericht.haxo2.discord.util.SerializableLocation;
import org.bukkit.Location;


import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
public class VaroConfigData {

    @Setter
    private VaroState varoState = VaroState.SETUP;

    private List<SerializableLocation> spawnLocations = Lists.newArrayList();

    private List<VaroTeam> varoTeams = Lists.newArrayList();

    private long millisLastEpisodeReset;

    private int episodesPerDay = 2;

    public VaroConfigData(){

    }

    public VaroConfig toVaroConfig(){
        List<Location> locations = new ArrayList<>();
        spawnLocations.forEach(serializableLocation -> locations.add(serializableLocation.toBukkitLocation()));
        return new VaroConfig(varoState, locations, varoTeams, millisLastEpisodeReset, episodesPerDay);
    }




}
