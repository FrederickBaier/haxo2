package me.wetterbericht.haxo2.bukkit.config;

import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.discord.util.JsonData;

import java.io.File;

public class VaroConfigDataFile {

    private File file = new File(Varo.PLUGIN_MAIN_DIR + "config.json");

    public VaroConfigData loadFile(){
        if (file.exists()){
            return JsonData.fromJsonFile(file).getObject("data", VaroConfigData.class);
        }
        return new VaroConfigData();
    }

    public void saveToFile(VaroConfigData varoConfigData){
        new JsonData().append("data", varoConfigData).saveAsFile(file);
    }

}
