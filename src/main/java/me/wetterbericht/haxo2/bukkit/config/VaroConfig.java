package me.wetterbericht.haxo2.bukkit.config;

import lombok.Getter;
import lombok.Setter;
import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import me.wetterbericht.haxo2.discord.util.SerializableLocation;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
public class VaroConfig {


    @Setter
    private VaroState varoState;

    private List<Location> spawnLocations;

    private List<VaroTeam> varoTeams;

    @Setter
    private long millisLastEpisodeReset;



    private int episodesPerDay = 2;

    public VaroConfig(VaroState varoState, List<Location> locations, List<VaroTeam> varoTeams, long millisLastEpisodeReset, int episodesPerDay){
        this.spawnLocations = locations;
        this.varoTeams = varoTeams;
        this.varoState = varoState;
        this.episodesPerDay = episodesPerDay;
        this.millisLastEpisodeReset = millisLastEpisodeReset;
    }

    public boolean enaughtSpawnsForNextTeam() {
        return ((varoTeams.size() + 1) * 2) <= spawnLocations.size();
    }

    public Location getSpawnForPlayer(UUID uuid) {
        boolean firstUser = false;
        int varoTeamIndex = -1;
        for (int i = 0; i < varoTeams.size(); i++) {
            VaroTeam varoTeam = varoTeams.get(i);
            if (varoTeam.getVaroPlayer1().getMinecraftUUID().equals(uuid)) {
                firstUser = true;
                varoTeamIndex = i;
                break;
            }
            if (varoTeam.getVaroPlayer2().getMinecraftUUID().equals(uuid)) {
                firstUser = false;
                varoTeamIndex = i;
                break;
            }
        }
        if (varoTeamIndex == -1)
            return null;
        int index = (varoTeamIndex * 2) + (firstUser ? 0 : 1);
        return spawnLocations.get(index);
    }

    public void addSpawn(Location location){
        spawnLocations.add(location);
    }

    public void addVaroTeam(VaroTeam varoTeam){
        this.varoTeams.add(varoTeam);
    }

    public VaroConfigData toVaroConfigData(){
        List<SerializableLocation> locations = new ArrayList<>();
        spawnLocations.forEach(location -> locations.add(new SerializableLocation(location)));
        return new VaroConfigData(varoState, locations, varoTeams, millisLastEpisodeReset, episodesPerDay);
    }

    public VaroTeam getPlayersTeam(UUID uuid){
        return varoTeams.stream().filter(varoTeam -> varoTeam.isPlayerInTeam(uuid)).findFirst().orElse(null);
    }

    public VaroTeam getUsersTeam(Long userId){
        return varoTeams.stream().filter(varoTeam -> varoTeam.isUserInTeam(userId)).findFirst().orElse(null);
    }

    public VaroTeam getVaroTeamByName(String name){
        return varoTeams.stream().filter(varoTeam -> varoTeam.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public List<VaroPlayer> getVaroPlayers(){
        List<VaroPlayer> varoPlayers = new ArrayList<>();
        varoTeams.forEach(varoTeam -> {
            varoPlayers.add(varoTeam.getVaroPlayer1());
            varoPlayers.add(varoTeam.getVaroPlayer2());
        });
        return varoPlayers;
    }

    public boolean isPlayerInAnyTeam(UUID uuid){
        return getPlayersTeam(uuid) != null;
    }

    public VaroPlayer getVaroPlayer(UUID uuid){
        for (VaroTeam varoTeam : varoTeams){
            if (varoTeam.getVaroPlayer(uuid) != null)
                return varoTeam.getVaroPlayer(uuid);
        }
        return null;
    }

    public boolean isUserInAnyTeam(Long userId){
        return getUsersTeam(userId) != null;
    }

}
