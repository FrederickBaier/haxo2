package me.wetterbericht.haxo2.bukkit.scheduler;

import me.wetterbericht.haxo2.bukkit.BukkitPluginMain;
import me.wetterbericht.haxo2.bukkit.Varo;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class ProtectionTimeScheduler {

    private HashMap<Player, Integer> map = new HashMap<>();


    public void addPlayer(Player p) {
        map.put(p, 10);
    }

    public boolean isPlayerInProtectionTime(Player player) {
        return map.containsKey(player);
    }

    public void run() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                for (Player p : map.keySet()) {
                    if (!p.isOnline()) {
                        map.remove(p);
                    }
                    int i = map.get(p);
                    if (i == 0) {
                        map.remove(p);
                        Bukkit.broadcastMessage(Varo.PREFIX + "Die Schutzzeit von §e" + p.getName() + " §7ist nun vorbei.");
                        return;
                    }
                    if (i == 10 || i == 5 || i == 4 || i == 3 || i == 2 || i == 1) {
                        Bukkit.broadcastMessage(Varo.PREFIX + "Die Schutzzeit von §e" + p.getName() + " §7endet in §e" + i + " §7Sekunden.");
                    }
                    i--;
                    map.put(p, i);
                }
            }
        }, 20, 20);
    }

}
