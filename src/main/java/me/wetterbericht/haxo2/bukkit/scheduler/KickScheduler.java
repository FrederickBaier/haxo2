package me.wetterbericht.haxo2.bukkit.scheduler;

import me.wetterbericht.haxo2.bukkit.BukkitPluginMain;
import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.PlayerDamagePlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import me.wetterbericht.haxo2.bukkit.scoreboard.IngameScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Date;

public class KickScheduler {

    private ArrayList<Player> playersWarnMessages = new ArrayList<>();

    public void run(){
        Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), () -> {
            checkEpisodeReset();
            checkKickPlayers();
            checkFood();
            Varo.getInstance().getLastDamager().removeAllUnnecessaryPlayers();
            playersWarnMessages.removeIf(player -> !player.isOnline());
        }, 10, 10);
    }

    private void checkFood() {
        if (Varo.getInstance().getVaroConfig().getVaroState() == VaroState.WAIT_FOR_START){
            Bukkit.getOnlinePlayers().forEach(player-> {
                player.setHealth(20);
                player.setFoodLevel(25);
                player.getActivePotionEffects().clear();
            });
        }
    }

    private void checkKickPlayers() {
        if (Varo.getInstance().getVaroConfig().getVaroState() != VaroState.RUNNING)
            return;
        for (Player p : Bukkit.getOnlinePlayers()){
            VaroTeam varoTeam = Varo.getInstance().getVaroConfig().getPlayersTeam(p.getUniqueId());
            VaroPlayer varoPlayer = varoTeam.getVaroPlayer(p.getUniqueId());
            IngameScoreboard.updateScoreboard(p, varoPlayer, varoTeam);
            long millisLeft = varoPlayer.getMillisLeft();
            if (millisLeft <= 0){
                if (Varo.getInstance().isInFight(p)) {
                    if (!playersWarnMessages.contains(p)) {
                        p.sendMessage(Varo.PREFIX + "§cDu befindest dich derzeit im Kampf. Solltest du dich aus dem Kampf entfernen wirst du automatisch gekickt.");
                        playersWarnMessages.add(p);
                    }
                    continue;
                }
                if (Varo.getInstance().isOtherPlayerTooClose(p)) {
                    if (!playersWarnMessages.contains(p)) {
                        p.sendMessage(Varo.PREFIX + "§cEin Gegener ist zu nah um das Spiel zu verlassen.");
                        playersWarnMessages.add(p);
                    }
                    continue;
                }
                playersWarnMessages.remove(p);
                varoPlayer.setMillisLeftOnQuit(0);
                p.kickPlayer(Varo.PREFIX + "Deine Zeit ist abgelaufen.");
            }
        }
    }

    private void checkEpisodeReset() {
        Date date = new Date(Varo.getInstance().getVaroConfig().getMillisLastEpisodeReset());
        Date currentData = new Date();
        if (date.getDay() != currentData.getDay()){
            Varo.getInstance().getVaroConfig().setMillisLastEpisodeReset(System.currentTimeMillis());
            for (VaroPlayer varoPlayer : Varo.getInstance().getVaroConfig().getVaroPlayers()){
                varoPlayer.setEpisodesAtDay(0);
            }
            Varo.getInstance().saveVaroConfig();
        }
    }

}
