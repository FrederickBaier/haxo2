package me.wetterbericht.haxo2.bukkit;

import lombok.Getter;
import me.wetterbericht.haxo2.bukkit.config.VaroConfig;
import me.wetterbericht.haxo2.bukkit.config.VaroConfigDataFile;
import me.wetterbericht.haxo2.bukkit.objects.PlayerDamagePlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import me.wetterbericht.haxo2.bukkit.utils.LastDamager;
import me.wetterbericht.haxo2.discord.util.BotUtils;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.File;

@Getter
public class Varo {

    @Getter
    private static Varo instance;

    public static String PREFIX = "§8»§a Haxo §8×§7 ";

    public static String PLUGIN_MAIN_DIR = "plugins/Haxo/";

    private VaroConfig varoConfig;

    private LastDamager lastDamager = new LastDamager();

    public static boolean PROTECTION_TIME = false;

    //TODO
    //
    //

    //alte map ? Texturepack ? 25 Blöcke pro Tod? regen?

    public Varo() {
        instance = this;
        this.varoConfig = new VaroConfigDataFile().loadFile().toVaroConfig();
        new File(PLUGIN_MAIN_DIR).mkdirs();
        if (getVaroConfig().getVaroState() == VaroState.WAIT_FOR_START)
            getVaroConfig().setVaroState(VaroState.REGISTER_PLAYER);
        if (getVaroConfig().getVaroState() != VaroState.RUNNING){
            Bukkit.getWorlds().get(0).setDifficulty(Difficulty.PEACEFUL);
        }
    }


    public void saveVaroConfig() {
        new VaroConfigDataFile().saveToFile(varoConfig.toVaroConfigData());
    }


    public boolean isOtherPlayerTooClose(Player p) {
        VaroTeam playersTeam = Varo.getInstance().getVaroConfig().getPlayersTeam(p.getUniqueId());
        for (Player all : Bukkit.getOnlinePlayers()) {
            if (distanceBetweenLocations(p.getLocation(), all.getLocation()) < 25) {
                VaroTeam teamAll = Varo.getInstance().getVaroConfig().getPlayersTeam(all.getUniqueId());
                if (!playersTeam.equals(teamAll)) {
                    return true;
                }
            }
        }
        return false;
    }

    public double distanceBetweenLocations(Location loc1, Location loc2) {
        return Math.sqrt(Math.pow(loc1.getX() - loc2.getX(), 2) + Math.pow(loc1.getY() - loc2.getY(), 2) + Math.pow(loc1.getZ() - loc2.getZ(), 2));
    }

    public void strikePlayer(Player p) {
        VaroPlayer varoPlayer = getVaroConfig().getVaroPlayer(p.getUniqueId());
        varoPlayer.addStrike();
        BotUtils.sendStrikeMessage(p, varoPlayer);
        switch (varoPlayer.getStrikes()){
            case 1:
                break;
            case 2:
                p.getInventory().clear();
                break;
            case 3:
                varoPlayer.setPlayerDead();
                break;
        }
    }

    public boolean isInFight(Player p){
        PlayerDamagePlayer playerDamagePlayer = getLastDamager().getPlayerDamagePlayer(p);
        if (playerDamagePlayer == null)
            return false;
        return (System.currentTimeMillis() - playerDamagePlayer.getTimeStamp()) < PlayerDamagePlayer.LOG_TIME;
    }


}
