package me.wetterbericht.haxo2.bukkit.command.commands;

import me.wetterbericht.haxo2.bukkit.BukkitPluginMain;
import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import me.wetterbericht.haxo2.discord.util.BotUtils;
import me.wetterbericht.haxo2.discord.util.ThreadUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.awt.*;

public class StartCommand implements CommandExecutor {

    private int count = 60 * 1; // 30 min

    private boolean countdownStarted = false;

    private int cd;



    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player))
            return true;
        Player p = (Player) sender;
        if (!p.hasPermission("haxo.setup"))
            return true;

        if (Varo.getInstance().getVaroConfig().getVaroState() != VaroState.REGISTER_PLAYER || countdownStarted) {
            p.sendMessage(Varo.PREFIX + "Dieser Command kann jetzt nicht ausgeführt werden.");
            return true;
        }
        countdownStarted = true;
        Varo.getInstance().getVaroConfig().setVaroState(VaroState.WAIT_FOR_START);
        p.sendMessage(Varo.PREFIX + "Der Countdown wird gestartet.");
        cd = Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (count == 60 * 30) {
                    Bukkit.broadcastMessage(Varo.PREFIX + "§eHaxo 2 §7startet in §e30 §7minuten.");
                    ThreadUtils.startDeamonThread(() -> BotUtils.sendMessageInGuild("countdown", new EmbedBuilder().setColor(Color.YELLOW)
                            .setTitle("Haxo 2 beginnt in 30 Minuten.").setDescription("Du kannst nun auf den Server joinen.").build()));
                }
                if (count == 60 * 20 || count == 60 * 15 || count == 60 * 10 || count == 60 * 5 || count == 60 * 2 || count == 60) {
                    int minutes = count / 60;
                    Bukkit.broadcastMessage(Varo.PREFIX + "§eHaxo 2 §7startet in §e" + minutes + " §7minuten.");
                    ThreadUtils.startDeamonThread(() -> BotUtils.sendMessageInGuild("countdown", new EmbedBuilder().setColor(Color.YELLOW)
                            .setTitle("Haxo 2 beginnt in " + minutes + " Minuten.").build()));
                }
                if (count == 50 || count == 40 || count == 30 || count == 20 || count == 10 || count == 5 || count == 4 || count == 3 || count == 2 || count == 1) {
                    Bukkit.broadcastMessage(Varo.PREFIX + "§eHaxo 2 §7startet in §e" + count + " §7Sekunden.");
                }
                if (count == 0) {
                    Bukkit.broadcastMessage(Varo.PREFIX + "§eHaxo 2 §7startet jetzt.");
                    Bukkit.broadcastMessage(Varo.PREFIX + "§eViel Glück.");
                    Bukkit.getScheduler().cancelTask(cd);
                    ThreadUtils.startDeamonThread(() -> BotUtils.sendMessageInGuild("countdown", new EmbedBuilder().setColor(Color.GREEN)
                            .setTitle("Haxo 2 hat nun begonnen.").build()));
                    if (Varo.getInstance().getVaroConfig().getVaroState() != VaroState.WAIT_FOR_START) {
                        p.sendMessage(Varo.PREFIX + "Es trat ein Fehler auf.");
                        return;
                    }
                    Varo.getInstance().getVaroConfig().setVaroState(VaroState.RUNNING);
                    Varo.getInstance().getVaroConfig().setMillisLastEpisodeReset(System.currentTimeMillis());
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        p.setFoodLevel(25);
                        p.setHealth(20);
                        p.setGameMode(GameMode.SURVIVAL);
                        VaroPlayer varoPlayer = Varo.getInstance().getVaroConfig().getVaroPlayer(p.getUniqueId());
                        if (varoPlayer != null) {
                            p.setOp(false);
                            varoPlayer.setEpisodesAtDay(1);
                            varoPlayer.addEpisode();
                            varoPlayer.setMillisLastJoin(System.currentTimeMillis());
                        }
                    }
                    Bukkit.getWorlds().get(0).setStorm(false);
                    Bukkit.getWorlds().get(0).getWorldBorder().setSize(4000);
                    Bukkit.getWorlds().get(0).setDifficulty(Difficulty.NORMAL);
                    Varo.getInstance().saveVaroConfig();

                    count = 60;
                    Varo.PROTECTION_TIME = true;
                    Bukkit.broadcastMessage(Varo.PREFIX + "§aDie Schutzzeit hat begonnen.");
                    Bukkit.broadcastMessage(Varo.PREFIX + "Die Schutzzeit endet in §e" + count + " §7Sekunden.");
                    cd = Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            count--;
                            if (count == 50 || count == 40 || count == 30 || count == 20 || count == 10 || count == 5 || count == 4 || count == 3 || count == 2 || count == 1)
                                Bukkit.broadcastMessage(Varo.PREFIX + "Die Schutzzeit endet in §e" + count + " §7Sekunden.");
                            if (count == 0) {
                                Bukkit.broadcastMessage(Varo.PREFIX + "§aDie Schutzzeit ist jetzt vorbei.");
                                Bukkit.getScheduler().cancelTask(cd);
                                Varo.PROTECTION_TIME = false;
                            }

                        }
                    }, 20, 20);
                }
                count--;
            }
        }, 20, 20);

        return true;
    }
}
