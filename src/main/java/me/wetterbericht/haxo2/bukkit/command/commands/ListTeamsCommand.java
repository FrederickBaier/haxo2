package me.wetterbericht.haxo2.bukkit.command.commands;

import com.mojang.authlib.GameProfile;
import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import me.wetterbericht.haxo2.bukkit.utils.ColorUtils;
import me.wetterbericht.haxo2.bukkit.utils.GameProfileBuilder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.io.IOException;

public class ListTeamsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        commandSender.sendMessage(Varo.PREFIX + "------ §eListe der Teams §7-----");
        for (VaroTeam varoTeam : Varo.getInstance().getVaroConfig().getVaroTeams()){
            GameProfile gameProfile1 = null;
            GameProfile gameProfile2 = null;
            try {
                gameProfile1 = GameProfileBuilder.fetch(varoTeam.getVaroPlayer1().getMinecraftUUID());
                gameProfile2 = GameProfileBuilder.fetch(varoTeam.getVaroPlayer2().getMinecraftUUID());
            } catch (IOException e) {
                commandSender.sendMessage(Varo.PREFIX + "Es trat ein Fehler bei Team §e" + varoTeam.getName() + " §7auf.");
                e.printStackTrace();
                continue;
            }
            commandSender.sendMessage(ColorUtils.toMcColor(varoTeam.getColor())  + varoTeam.getName() + "§7: §e" +
                   (varoTeam.getVaroPlayer1().isAlive() ? "" : "§m") + gameProfile1.getName() + " " +
                    (varoTeam.getVaroPlayer2().isAlive() ? "" : "§m") + gameProfile2.getName());
        }
        return true;
    }
}
