package me.wetterbericht.haxo2.bukkit.command;

import me.wetterbericht.haxo2.bukkit.command.commands.FinishCommand;
import me.wetterbericht.haxo2.bukkit.command.commands.ListTeamsCommand;
import me.wetterbericht.haxo2.bukkit.command.commands.SetSpawnCommand;
import me.wetterbericht.haxo2.bukkit.command.commands.StartCommand;
import org.bukkit.plugin.java.JavaPlugin;

public class CommandManager {

    public CommandManager(JavaPlugin javaPlugin){
        javaPlugin.getCommand("setspawn").setExecutor(new SetSpawnCommand());
        javaPlugin.getCommand("finish").setExecutor(new FinishCommand());
        javaPlugin.getCommand("start").setExecutor(new StartCommand());
        javaPlugin.getCommand("listteams").setExecutor(new ListTeamsCommand());
    }

}
