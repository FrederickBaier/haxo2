package me.wetterbericht.haxo2.bukkit.command.commands;


import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FinishCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player))
            return true;
        Player p = (Player) sender;
        if (!p.hasPermission("haxo.setup"))
            return true;
        if (Varo.getInstance().getVaroConfig().getVaroState() != VaroState.SETUP){
            p.sendMessage(Varo.PREFIX + "Dieser Command kann jetzt nicht ausgeführt werden.");
            return true;
        }
        p.sendMessage(Varo.PREFIX + "Die Teilnehmer können sich nun registrieren.");
        Varo.getInstance().getVaroConfig().setVaroState(VaroState.REGISTER_PLAYER);
        Varo.getInstance().saveVaroConfig();
        return true;
    }
}
