package me.wetterbericht.haxo2.bukkit.command.commands;

import me.wetterbericht.haxo2.bukkit.Varo;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawnCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player))
            return true;
        Player p = (Player) sender;
        if (!p.hasPermission("haxo.setup"))
            return true;
        Varo.getInstance().getVaroConfig().addSpawn(p.getLocation());
        p.sendMessage(Varo.PREFIX + "Spawn hinzugefügt. (" + Varo.getInstance().getVaroConfig().getSpawnLocations().size() + ")");
        Varo.getInstance().saveVaroConfig();

        return true;
    }
}
