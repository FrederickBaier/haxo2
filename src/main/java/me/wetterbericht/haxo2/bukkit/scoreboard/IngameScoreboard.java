package me.wetterbericht.haxo2.bukkit.scoreboard;

import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import me.wetterbericht.haxo2.bukkit.scoreboardapi.SimplePlayerScoreboardManager;
import me.wetterbericht.haxo2.bukkit.scoreboardapi.SimpleScoreboard;
import me.wetterbericht.haxo2.bukkit.utils.ColorUtils;
import me.wetterbericht.haxo2.bukkit.utils.MillisTimeUtils;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IngameScoreboard {

    private static SimplePlayerScoreboardManager simplePlayerScoreboardManager = new SimplePlayerScoreboardManager();

    /*
    7 Team:
    6 <team>
    5 Kills:
    4 <kills>
    3 Episode:
    2 <episode>
    1 Strikes:
    0 <strikes>


     */


    public static void updateScoreboard(Player p, VaroPlayer varoPlayer, VaroTeam varoTeam) {
        if (simplePlayerScoreboardManager.hasScoreboard(p)) {
            SimpleScoreboard simpleScoreboard = simplePlayerScoreboardManager.getScorebaord(p);
            simpleScoreboard.getFirstScoreboardEntry(9).setChangeableText("§e" + MillisTimeUtils.getTimeAsString(varoPlayer.getMillisLeft()));
            simpleScoreboard.getFirstScoreboardEntry(6).setChangeableText("§e" + varoPlayer.getKills());
            simpleScoreboard.getFirstScoreboardEntry(3).setChangeableText("§e" + varoPlayer.getEpisode());
            simpleScoreboard.getFirstScoreboardEntry(0).setChangeableText("§e" + varoPlayer.getStrikes());
        } else {
            SimpleScoreboard simpleScoreboard = simplePlayerScoreboardManager.getNewScoreboard(p, "§8»§a Haxo §8«");
            simpleScoreboard.addScoreboardEntry(11, "§1");
            simpleScoreboard.addScoreboardEntry(10, "§7Zeit§8:");
            if (varoPlayer.getMillisLeft() > -1) {
                simpleScoreboard.addScoreboardEntry(9, "§8» ", "§e" + MillisTimeUtils.getTimeAsString(varoPlayer.getMillisLeft()));
            } else {
                simpleScoreboard.addScoreboardEntry(9, "§8» ", "§e0:00");
            }
            simpleScoreboard.addScoreboardEntry(8, "§2");
            simpleScoreboard.addScoreboardEntry(7, "§7Kills§8:");
            simpleScoreboard.addScoreboardEntry(6, "§8» ", "§e" + varoPlayer.getKills());
            simpleScoreboard.addScoreboardEntry(5, "§3");
            simpleScoreboard.addScoreboardEntry(4, "§7Episode§8:");
            simpleScoreboard.addScoreboardEntry(3, "§8» ", "§e" + varoPlayer.getEpisode());
            simpleScoreboard.addScoreboardEntry(2, "§4");
            simpleScoreboard.addScoreboardEntry(1, "§7Strikes§8:");
            simpleScoreboard.addScoreboardEntry(0, "§8» ", "§e" + varoPlayer.getStrikes());
        }
    }


}
