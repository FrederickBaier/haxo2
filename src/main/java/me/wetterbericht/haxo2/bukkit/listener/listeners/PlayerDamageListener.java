package me.wetterbericht.haxo2.bukkit.listener.listeners;

import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerDamageListener implements Listener {

    @EventHandler
    public void on(EntityDamageEvent e){
        if (!(e.getEntity() instanceof Player))
            return;
        if (Varo.getInstance().getVaroConfig().getVaroState() != VaroState.RUNNING){
            e.setCancelled(true);
        }
    }

}
