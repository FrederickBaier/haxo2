package me.wetterbericht.haxo2.bukkit.listener.listeners;

import me.wetterbericht.haxo2.bukkit.BukkitPluginMain;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;
import java.util.Collection;

public class CraftItemListener implements Listener {

    @EventHandler
    public void on(CraftItemEvent e) {
        ItemStack itemStack = e.getInventory().getResult();
        if (itemStack == null)
            return;
        if (itemStack.getType() == Material.GOLDEN_APPLE && itemStack.getData().getData() == 1)
            e.setCancelled(true);
    }


    @EventHandler
    public void on(BrewEvent e) {
        ItemStack[] contents = new ItemStack[e.getContents().getContents().length];
        for (int i = 0; i < contents.length; i++) {
            if (e.getContents().getContents()[i] != null)
                contents[i] = e.getContents().getContents()[i].clone();
        }


        Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < e.getContents().getContents().length - 1; i++) {
                    Potion potion = Potion.fromItemStack(e.getContents().getItem(i));
                    System.out.println("------");
                    potion.getEffects().forEach(potionEffect -> System.out.println(potionEffect.getType()));
                    System.out.println(potion.isSplash());
                    if (hasPotionEffect(potion, Arrays.asList(PotionEffectType.FIRE_RESISTANCE, PotionEffectType.INCREASE_DAMAGE, PotionEffectType.HARM))) {
                        e.getContents().setContents(contents);
                        return;
                    }
                    if (potion.isSplash()) {
                        if (hasPotionEffect(potion, Arrays.asList(PotionEffectType.HEAL, PotionEffectType.REGENERATION))) {
                            e.getContents().setContents(contents);
                            return;
                        }
                    }

                }
            }
        }, 1);
    }


    public boolean hasPotionEffect(Potion potion, Collection<PotionEffectType> potionEffectType) {
        for (PotionEffect potionEffect : potion.getEffects()) {
            if (potionEffectType.contains(potionEffect.getType()))
                return true;
        }
        return false;
    }
}
