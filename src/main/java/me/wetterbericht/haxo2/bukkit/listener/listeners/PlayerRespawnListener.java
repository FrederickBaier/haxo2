package me.wetterbericht.haxo2.bukkit.listener.listeners;

import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerRespawnListener implements Listener {


    @EventHandler
    public void on(PlayerRespawnEvent e){
        if (Varo.getInstance().getVaroConfig().getVaroState() != VaroState.RUNNING){
            return;
        }
        Player p = e.getPlayer();
        VaroTeam varoTeam = Varo.getInstance().getVaroConfig().getPlayersTeam(p.getUniqueId());
        if (varoTeam == null)
            return;
        p.kickPlayer(Varo.PREFIX + "Du bist gestorben. Damit bist du aus Haxo 2 ausgeschieden.");
    }


}
