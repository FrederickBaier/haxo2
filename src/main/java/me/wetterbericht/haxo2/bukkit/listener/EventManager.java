package me.wetterbericht.haxo2.bukkit.listener;

import me.wetterbericht.haxo2.bukkit.listener.listeners.*;
import org.bukkit.plugin.java.JavaPlugin;

public class EventManager {

    public EventManager(JavaPlugin javaPlugin){
        javaPlugin.getServer().getPluginManager().registerEvents(new PlayerLoginListener(), javaPlugin);
        javaPlugin.getServer().getPluginManager().registerEvents(new PlayerJoinListener(), javaPlugin);
        javaPlugin.getServer().getPluginManager().registerEvents(new PlayerQuitKickListener(), javaPlugin);
        javaPlugin.getServer().getPluginManager().registerEvents(new PlayerChatListener(), javaPlugin);
        javaPlugin.getServer().getPluginManager().registerEvents(new PlayerDamageListener(), javaPlugin);
        javaPlugin.getServer().getPluginManager().registerEvents(new EntityDamageByEntityListener(), javaPlugin);
        javaPlugin.getServer().getPluginManager().registerEvents(new PlayerDeathListener(), javaPlugin);
        javaPlugin.getServer().getPluginManager().registerEvents(new PlayerRespawnListener(), javaPlugin);
        javaPlugin.getServer().getPluginManager().registerEvents(new CraftItemListener(), javaPlugin);
    }

}
