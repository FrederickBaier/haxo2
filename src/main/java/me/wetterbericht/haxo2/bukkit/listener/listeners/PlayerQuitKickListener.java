package me.wetterbericht.haxo2.bukkit.listener.listeners;

import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import me.wetterbericht.haxo2.discord.util.BotUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitKickListener implements Listener {

    @EventHandler
    public void on(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        e.setQuitMessage("§8<< §e" + p.getName() + "§7 hat das Spiel verlassen.");
        if (Varo.getInstance().getVaroConfig().getVaroState() == VaroState.RUNNING) {
            VaroPlayer varoPlayer = Varo.getInstance().getVaroConfig().getVaroPlayer(p.getUniqueId());
            if (varoPlayer.getMillisLeftOnQuit() == 0)
                return;
            varoPlayer.setMillisLeftOnQuit(varoPlayer.getMillisLeft());
            if (Varo.getInstance().isOtherPlayerTooClose(p) || Varo.getInstance().isInFight(p)){
                Varo.getInstance().strikePlayer(p);
            }
        }
    }

    @EventHandler
    public void on(PlayerKickEvent e){
        Player p = e.getPlayer();
        VaroPlayer varoPlayer = Varo.getInstance().getVaroConfig().getVaroPlayer(p.getUniqueId());
        if (varoPlayer != null){
            if (varoPlayer.getEpisode() == 15 && !varoPlayer.isEnemyContact()){
                varoPlayer.setEnemyContact(true);
                Varo.getInstance().strikePlayer(p);
            }
        }
    }

}
