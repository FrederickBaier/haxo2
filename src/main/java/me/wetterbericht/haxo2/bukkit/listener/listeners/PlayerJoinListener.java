package me.wetterbericht.haxo2.bukkit.listener.listeners;

import me.wetterbericht.haxo2.bukkit.BukkitPluginMain;
import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import me.wetterbericht.haxo2.bukkit.utils.ColorUtils;
import me.wetterbericht.haxo2.bukkit.utils.MillisTimeUtils;
import me.wetterbericht.haxo2.discord.util.BotUtils;
import me.wetterbericht.haxo2.discord.util.ThreadUtils;
import net.dv8tion.jda.api.EmbedBuilder;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class PlayerJoinListener implements Listener {

    private List<Player> inProtectionTime = new ArrayList<>();

    @EventHandler
    public void on(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        VaroTeam varoTeam = Varo.getInstance().getVaroConfig().getPlayersTeam(p.getUniqueId());
        switch (Varo.getInstance().getVaroConfig().getVaroState()) {
            case REGISTER_PLAYER:
                if (!p.hasPermission("haxo.setup")) {
                    Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> {
                        p.setGameMode(GameMode.ADVENTURE);
                        p.getInventory().clear();
                        p.getInventory().setArmorContents(null);
                        p.setExp(0);
                        p.setLevel(0);
                    }, 10);
                }
                break;
            case WAIT_FOR_START:
                Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> {
                    p.teleport(Varo.getInstance().getVaroConfig().getSpawnForPlayer(p.getUniqueId()));
                    p.setGameMode(GameMode.ADVENTURE);
                    p.getInventory().clear();
                    p.getInventory().setArmorContents(null);
                    p.setExp(0);
                    p.setLevel(0);
                }, 4);
                e.setJoinMessage("§8>> §e" + p.getName() + " §7aus Team §e" + varoTeam.getName() + " §7hat den Server betreten.");
                p.setPlayerListName(ColorUtils.toMcColor(varoTeam.getColor()) + varoTeam.getName() + " §8|§7 " + p.getName());
                break;
            case RUNNING:
                VaroPlayer varoPlayer = varoTeam.getVaroPlayer(p.getUniqueId());
                p.setPlayerListName(ColorUtils.toMcColor(varoTeam.getColor()) + varoTeam.getName() + " §8|§7 " + p.getName());
                if (varoPlayer.getMillisLeftOnQuit() != 0) {
                    String timeLeft = MillisTimeUtils.getTimeAsString(varoPlayer.getMillisLeftOnQuit());
                    e.setJoinMessage("§8>> §e" + p.getName() + " §7aus Team §e" + varoTeam.getName() +
                            " §7hat den Server wieder betreten. Er hat noch §e" + timeLeft + " §7Minuten.");
                    varoPlayer.setMillisLastJoin(System.currentTimeMillis());
                    ThreadUtils.startDeamonThread(() -> BotUtils.sendMessageInGuild("join-infos", new EmbedBuilder().setColor(Color.YELLOW).setTitle(p.getName() + " (" + varoPlayer.getDiscordUser().getName() + ") aus Team " + varoTeam.getName() + " hat den Server wieder betreten.")
                            .setDescription("Er hat noch " + timeLeft + " Minuten").build()));
                    return;
                }
                varoPlayer.addEpisode();
                varoPlayer.setEpisodesAtDay(varoPlayer.getEpisodesAtDay() + 1);
                varoPlayer.setMillisLastJoin(System.currentTimeMillis());
                varoPlayer.setMillisLeftOnQuit(-1);
                e.setJoinMessage("§8>> §e" + p.getName() + " §7aus Team §e" + varoTeam.getName() + " §7hat den Server betreten.");
                startProtectionTime(p);
                ThreadUtils.startDeamonThread(() -> BotUtils.sendMessageInGuild("join-infos", new EmbedBuilder().setColor(Color.YELLOW).setTitle(p.getName() + " (" + varoPlayer.getDiscordUser().getName() + ") aus Team " + varoTeam.getName() + " hat den Server betreten.").build()));

        }
    }

    private void startProtectionTime(Player player) {
        BukkitPluginMain.getInstance().getProtectionTimeScheduler().addPlayer(player);
    }

    @EventHandler
    public void on(BlockBreakEvent e) {
        if (BukkitPluginMain.getInstance().getProtectionTimeScheduler().isPlayerInProtectionTime(e.getPlayer()))
            e.setCancelled(true);
    }

    @EventHandler
    public void on(PlayerDropItemEvent e) {
        if (BukkitPluginMain.getInstance().getProtectionTimeScheduler().isPlayerInProtectionTime(e.getPlayer()))
            e.setCancelled(true);
    }

    @EventHandler
    public void on(PlayerInteractEvent e) {
        if (BukkitPluginMain.getInstance().getProtectionTimeScheduler().isPlayerInProtectionTime(e.getPlayer()))
            e.setCancelled(true);
    }

    @EventHandler
    public void on(InventoryInteractEvent e) {
        if (BukkitPluginMain.getInstance().getProtectionTimeScheduler().isPlayerInProtectionTime((Player) e.getWhoClicked()))
            e.setCancelled(true);
    }

    @EventHandler
    public void on(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player))
            return;
        if (BukkitPluginMain.getInstance().getProtectionTimeScheduler().isPlayerInProtectionTime((Player) e.getEntity()))
            e.setCancelled(true);
    }

    @EventHandler
    public void on(PlayerMoveEvent e) {
        if (e.getTo().getBlockX() != e.getFrom().getBlockX() || e.getTo().getBlockZ() != e.getFrom().getBlockZ())
            if (BukkitPluginMain.getInstance().getProtectionTimeScheduler().isPlayerInProtectionTime(e.getPlayer()))
                e.getPlayer().teleport(e.getFrom());
    }

}
