package me.wetterbericht.haxo2.bukkit.listener.listeners;


import me.wetterbericht.haxo2.bukkit.BukkitPluginMain;
import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.PlayerDamagePlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import me.wetterbericht.haxo2.bukkit.utils.ColorUtils;
import me.wetterbericht.haxo2.discord.util.BotUtils;
import me.wetterbericht.haxo2.discord.util.ThreadUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.awt.*;

public class PlayerDeathListener implements Listener {

    private int endCD = 60 * 5;

    @EventHandler
    public void on(PlayerDeathEvent e) {
        Player p = e.getEntity();
        VaroTeam varoTeam = Varo.getInstance().getVaroConfig().getPlayersTeam(p.getUniqueId());
        e.setDeathMessage(null);
        if (varoTeam == null) {
            e.setKeepInventory(true);
            e.setKeepLevel(true);
            System.out.println("Team was null");
            return;
        }
        e.setKeepInventory(false);
        e.setKeepLevel(false);
        VaroPlayer varoPlayer = varoTeam.getVaroPlayer(p.getUniqueId());
        varoPlayer.setPlayerDead();
        if (!Varo.getInstance().isInFight(p)) {
           Bukkit.broadcastMessage(ColorUtils.toMcColor(varoTeam.getColor()) + p.getName() + " §7ist gestorben.");
        } else {
            PlayerDamagePlayer playerDamagePlayer = Varo.getInstance().getLastDamager().getPlayerDamagePlayer(p);
            VaroTeam damagerTeam = Varo.getInstance().getVaroConfig().getPlayersTeam(playerDamagePlayer.getDamager().getUniqueId());
            if (damagerTeam == null) {
                Bukkit.broadcastMessage(ColorUtils.toMcColor(varoTeam.getColor()) + p.getName() + " §7wurde von §8" + playerDamagePlayer.getDamager().getName() + " §7getötet.");
            } else {
                VaroPlayer damagerVaroPlayer = damagerTeam.getVaroPlayer(playerDamagePlayer.getDamager().getUniqueId());
                damagerVaroPlayer.addKill();
                Bukkit.broadcastMessage(ColorUtils.toMcColor(varoTeam.getColor()) + p.getName() + " §7wurde von " + ColorUtils.toMcColor(damagerTeam.getColor()) + playerDamagePlayer.getDamager().getName() + " §7getötet.");
            }
        }
        World world = Bukkit.getWorlds().get(0);
        System.out.println(world);
        world.getWorldBorder().setSize(world.getWorldBorder().getSize() - 25, 10);
        for (Player all : Bukkit.getOnlinePlayers()){
            all.playSound(all.getLocation(), Sound.AMBIENCE_THUNDER, 1, 1);
        }
        checkWin();
    }

    private void checkWin() {
        VaroTeam winner = null;
        for (VaroTeam varoTeam : Varo.getInstance().getVaroConfig().getVaroTeams()){
            if (varoTeam.getVaroPlayer1().isAlive() || varoTeam.getVaroPlayer2().isAlive()){
                if (winner != null){
                    winner = null;
                    break;
                }
                winner = varoTeam;
            }
        }
        if (winner == null)
            return;
        Varo.getInstance().getVaroConfig().setVaroState(VaroState.END);
        Bukkit.broadcastMessage(Varo.PREFIX + "Das Team " + ColorUtils.toMcColor(winner.getColor()) + winner.getName() + " §7hat Haxo 2 gewonnen.");
        BotUtils.sendMessageInGuild("countdown", new EmbedBuilder().setColor(Color.GREEN).setTitle("Das Team " + winner.getName() + " hat Haxo 2 gewonnen.").build());
        Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                endCD--;
                if (endCD == 60 || endCD == 50 || endCD == 40 || endCD == 30 || endCD == 20 || endCD == 10 || endCD == 5 || endCD == 4 || endCD == 3 || endCD == 2 || endCD == 1){
                    Bukkit.broadcastMessage(Varo.PREFIX + "Der Server stoppt in " + endCD + " Sekunden.");
                }
                if (endCD == 0){
                    Bukkit.getServer().shutdown();
                    Bukkit.getOnlinePlayers().forEach(player-> player.kickPlayer(Varo.PREFIX + "§cDer Server stoppt jetzt."));
                }
            }
        }, 20, 20);
    }

    @EventHandler
    public void on2(PlayerDeathEvent e){
        Player p = e.getEntity();
        Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (p.isOnline())
                    p.kickPlayer(Varo.PREFIX + "Du bist gestorben. Damit bist du aus Haxo 2 ausgeschieden.");
            }
        }, 20 * 6);
    }

}
