package me.wetterbericht.haxo2.bukkit.listener.listeners;

import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.VaroState;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageByEntityListener implements Listener {

    @EventHandler
    public void on(EntityDamageByEntityEvent e) {
        if (Varo.getInstance().getVaroConfig().getVaroState() != VaroState.RUNNING){
            e.setCancelled(true);
            return;
        }

        if (!(e.getEntity() instanceof Player) || !(e.getDamager() instanceof Player))
            return;
        if (Varo.PROTECTION_TIME){
            e.setCancelled(true);
            return;
        }
        Player p = (Player) e.getEntity();
        Player damager = (Player) e.getDamager();
        VaroTeam varoTeamPlayer = Varo.getInstance().getVaroConfig().getPlayersTeam(p.getUniqueId());
        VaroTeam varoTeamDamager = Varo.getInstance().getVaroConfig().getPlayersTeam(damager.getUniqueId());
        if (varoTeamDamager.equals(varoTeamPlayer)) {
            e.setCancelled(true);
        } else {
            Varo.getInstance().getLastDamager().putLastDamager(p, damager);
            varoTeamPlayer.getVaroPlayer(p.getUniqueId()).setEnemyContact(true);
            varoTeamDamager.getVaroPlayer(damager.getUniqueId()).setEnemyContact(true);
        }
    }

}
