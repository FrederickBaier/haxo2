package me.wetterbericht.haxo2.bukkit.listener.listeners;

import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.VaroPlayer;
import me.wetterbericht.haxo2.discord.DiscordBotMain;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class PlayerLoginListener implements Listener {

    @EventHandler
    public void on(PlayerLoginEvent e) {
        Player p = e.getPlayer();
        switch (Varo.getInstance().getVaroConfig().getVaroState()) {
            case SETUP:
                if (!p.hasPermission("haxo.setup")) {
                    e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Varo.PREFIX + "Der Server wird noch eingerichtet. \n §7Bitte komme später wieder.");
                    return;
                }
                break;
            case END:
                if (!p.hasPermission("haxo.setup")) {
                    e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Varo.PREFIX + "Haxo 2 ist bereits vorbei.");
                    return;
                }
                break;
            case WAIT_FOR_START:
                if (!p.hasPermission("haxo.setup") && Varo.getInstance().getVaroConfig().getPlayersTeam(p.getUniqueId()) == null) {
                    e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Varo.PREFIX + "Du bist nicht bei Haxo 2 registriert. \n Account: " + p.getName());
                    return;
                }
                break;
            case REGISTER_PLAYER:
                if (!p.hasPermission("haxo.setup") && (Varo.getInstance().getVaroConfig().getPlayersTeam(p.getUniqueId()) != null || DiscordBotMain.getInstance().getSetupManager().isPlayerRegistered(p.getUniqueId()))) {
                    e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Varo.PREFIX + "Du bist bereits registriert.");
                    return;
                }
                break;
            case RUNNING:
                if (!p.hasPermission("haxo.setup") && Varo.getInstance().getVaroConfig().getPlayersTeam(p.getUniqueId()) == null) {
                    e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Varo.PREFIX + "Du bist nicht bei Haxo 2 registriert. \n Account: " + p.getName());
                    return;
                }
                if (!Varo.getInstance().getVaroConfig().getVaroPlayer(p.getUniqueId()).isAlive()) {
                    e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Varo.PREFIX + "Du bist bereits aus Haxo 2 ausgeschieden.");
                    return;
                }
                VaroPlayer varoPlayer = Varo.getInstance().getVaroConfig().getVaroPlayer(p.getUniqueId());
                if (varoPlayer.getEpisodesAtDay() == Varo.getInstance().getVaroConfig().getEpisodesPerDay() && varoPlayer.getMillisLeftOnQuit() == 0) {
                    e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Varo.PREFIX + "Du hast die maximale Anzahl an Folgen für heute erreicht.");
                    return;
                }
        }
    }


}
