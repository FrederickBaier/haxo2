package me.wetterbericht.haxo2.bukkit.listener.listeners;

import me.wetterbericht.haxo2.bukkit.Varo;
import me.wetterbericht.haxo2.bukkit.objects.VaroTeam;
import me.wetterbericht.haxo2.bukkit.utils.ColorUtils;
import me.wetterbericht.haxo2.discord.DiscordBotMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.UUID;


public class PlayerChatListener implements Listener {


    @EventHandler
    public void on(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        switch (Varo.getInstance().getVaroConfig().getVaroState()) {
            case REGISTER_PLAYER:
                try {
                    UUID uuid = UUID.fromString(e.getMessage());
                    DiscordBotMain.getInstance().getSetupManager().onMcVerifyCodeAsync(p, uuid);
                    e.setCancelled(true);
                } catch (IllegalArgumentException ex) {
                    if (!p.hasPermission("haxo.setup")) {
                        p.sendMessage(Varo.PREFIX + "Du darfst keine Nachrichten schreiben.");
                        e.setCancelled(true);
                        return;
                    }
                }
                break;
            case WAIT_FOR_START:
            case RUNNING:
            case END:
                VaroTeam varoTeam = Varo.getInstance().getVaroConfig().getPlayersTeam(p.getUniqueId());
                e.setCancelled(true);
                Bukkit.broadcastMessage(ColorUtils.toMcColor(varoTeam.getColor()) + p.getName() + "§8: §7" + e.getMessage());
                break;
        }

    }
}
