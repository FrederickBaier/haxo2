package me.wetterbericht.haxo2.bukkit;

import lombok.Getter;
import me.wetterbericht.haxo2.bukkit.command.CommandManager;
import me.wetterbericht.haxo2.bukkit.listener.EventManager;
import me.wetterbericht.haxo2.bukkit.scheduler.KickScheduler;
import me.wetterbericht.haxo2.bukkit.scheduler.ProtectionTimeScheduler;
import me.wetterbericht.haxo2.discord.DiscordBotMain;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class BukkitPluginMain extends JavaPlugin {

    @Getter
    private static BukkitPluginMain instance;

    private ProtectionTimeScheduler protectionTimeScheduler;

    @Override
    public void onEnable() {
        instance = this;
        new DiscordBotMain();
        new Varo();
        new CommandManager(this);
        new EventManager(this);
        protectionTimeScheduler = new ProtectionTimeScheduler();
        protectionTimeScheduler.run();
        new KickScheduler().run();
    }

    @Override
    public void onDisable() {
        Varo.getInstance().saveVaroConfig();
    }
}
