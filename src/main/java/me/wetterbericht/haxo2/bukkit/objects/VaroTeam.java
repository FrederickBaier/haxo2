package me.wetterbericht.haxo2.bukkit.objects;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.awt.*;
import java.util.UUID;

@Getter
@AllArgsConstructor
public class VaroTeam {

   private String name;

   private Color color;

   private VaroPlayer varoPlayer1;

   private VaroPlayer varoPlayer2;

   public VaroTeam(String name, Color color){
      this.name = name;
      this.color = color;
   }

   public VaroTeam setVaroPlayer1(VaroPlayer varoPlayer1) {
      this.varoPlayer1 = varoPlayer1;
      return this;
   }

   public VaroTeam setVaroPlayer2(VaroPlayer varoPlayer2) {
      this.varoPlayer2 = varoPlayer2;
      return this;
   }

   public VaroPlayer getVaroPlayer(UUID uuid){
      if (varoPlayer1.getMinecraftUUID().equals(uuid))
         return varoPlayer1;
      if (varoPlayer2.getMinecraftUUID().equals(uuid))
         return varoPlayer2;
      return null;
   }

   public boolean isPlayerInTeam(UUID uuid){
      return getVaroPlayer(uuid) != null;
   }

   public boolean isUserInTeam(Long userID){
      return varoPlayer1.getDicordUserID() == userID|| varoPlayer2.getDicordUserID() == userID;
   }
}
