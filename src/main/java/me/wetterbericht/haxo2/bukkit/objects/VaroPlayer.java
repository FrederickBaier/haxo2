package me.wetterbericht.haxo2.bukkit.objects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import me.wetterbericht.haxo2.discord.DiscordBotMain;
import net.dv8tion.jda.api.entities.User;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class VaroPlayer {

    private long dicordUserID;

    private UUID minecraftUUID;

    private boolean isAlive = true;

    @Setter
    private boolean enemyContact = false;

    private int strikes = 0;

    private int episode = 0;

    private int kills;

    @Setter
    private int episodesAtDay = 0;

    /**
     * will be only set if a player lost the connection
     */
    @Setter
    private long millisLeftOnQuit = -1;

    @Setter
    private long millisLastJoin;

    public void addEpisode(){
        episode++;
    }

    public long getMillisLeft(){
        return millisLeftOnQuit == -1 ? (2 * 60 * 1000) - (System.currentTimeMillis() - millisLastJoin) : millisLeftOnQuit - (System.currentTimeMillis() - millisLastJoin);
    }

    public void addStrike(){
        strikes +=1;
    }

    public void removeStrike(){
        strikes -=1;
    }

    public VaroPlayer(long dicordUserID, UUID minecraftUUID){
        this.dicordUserID = dicordUserID;
        this.minecraftUUID = minecraftUUID;
    }

    public VaroPlayer setDiscordUserID(long dicordUserID){
        this.dicordUserID = dicordUserID;
        return this;
    }

    public VaroPlayer setMinecraftUUID(UUID minecraftUUID) {
        this.minecraftUUID = minecraftUUID;
        return this;
    }

    public void addKill(){
        kills++;
    }

    public void setPlayerDead(){
        this.isAlive = false;
    }

    public User getDiscordUser(){
        return DiscordBotMain.getInstance().getJda().getUserById(dicordUserID);
    }

}
