package me.wetterbericht.haxo2.bukkit.objects;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.bukkit.entity.Player;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class PlayerDamagePlayer {

    public static final long LOG_TIME = 1000 * 18;

    private Player player;

    private Player damager;

    private long timeStamp;


}
