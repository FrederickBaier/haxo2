package me.wetterbericht.haxo2.bukkit.utils;

import me.wetterbericht.haxo2.bukkit.objects.PlayerDamagePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class LastDamager {


    private ArrayList<PlayerDamagePlayer> playerDamagePlayers = new ArrayList<>();


    public void putLastDamager(Player p, Player damager){
        playerDamagePlayers.removeIf(playerDamagePlayer -> playerDamagePlayer.getPlayer().equals(p));
        playerDamagePlayers.add(new PlayerDamagePlayer(p, damager, System.currentTimeMillis()));
    }

    public PlayerDamagePlayer getPlayerDamagePlayer(Player player){
        return playerDamagePlayers.stream().filter(playerDamagePlayer -> playerDamagePlayer.getPlayer().equals(player)).findFirst().orElse(null);
    }


    public void removeAllUnnecessaryPlayers(){
        playerDamagePlayers.removeIf(playerDamagePlayer -> !playerDamagePlayer.getPlayer().isOnline());
    }



}
