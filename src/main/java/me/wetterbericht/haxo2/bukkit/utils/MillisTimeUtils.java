package me.wetterbericht.haxo2.bukkit.utils;

public class MillisTimeUtils {


    public static int[] getTime(long millis){
        int seconds = (int) millis / 1000;
        int minutes = 0;
        while (seconds >= 60){
            seconds-= 60;
            minutes++;
        }
        return new int[]{minutes, seconds};
    }

    public static String getTimeAsString(long millis){
        boolean millisNegative = millis < 0;
        if (millisNegative){
            millis *= -1;
        }
        int[] time = getTime(millis);
        if (time[1] < 10)
            return millisNegative ? "-" : "" + time[0] + ":0" + time[1];
        return millisNegative ? "-" : "" + time[0] + ":" + time[1];
    }

}
