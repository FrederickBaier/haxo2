package me.wetterbericht.haxo2.bukkit.utils;

import java.awt.*;

public class ColorUtils {


    public static String toMcColor(Color color){
        if (color.equals(Color.WHITE))
            return "§f";
        if (color.equals(Color.GRAY))
            return "§7";
        if (color.equals(Color.BLACK))
            return "§0";
        if (color.equals(Color.RED))
            return "§4";
        if (color.equals(Color.PINK))
            return "§d";
        if (color.equals(Color.ORANGE))
            return "§6";
        if (color.equals(Color.YELLOW))
            return "§e";
        if (color.equals(Color.GREEN))
            return "§a";
        if (color.equals(Color.MAGENTA))
            return "§5";
        if (color.equals(Color.CYAN))
            return "§b";
        if (color.equals(Color.BLUE))
            return "§9";

        return "§f";
    }

}
